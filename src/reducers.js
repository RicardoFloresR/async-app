import {SET_SEARCH, STARTING_FETCH, SET_RESPONSE} from './constants';
export const appReducer = (state={}, action) => {
	switch (action.type) {
		case SET_SEARCH: 
			return {
				...state,
				search: action.playload
			};
		case STARTING_FETCH:
			return {
				...state,
				isFetching: ! state.isFetching
			};
		case SET_RESPONSE:
			return {
				...state,
				imgs: action.playload
			};
		default:
			return state;
	}
}