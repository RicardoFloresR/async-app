import React, {Fragment} from 'react';

const ImageResults = ({imgs}) => {
	return (
		<Fragment>
			{imgs.map((img, index) => <div className='img-container' key={`unsplash-img-${index}`}>
				<img src={img.url}/>
				</div>)}
		</Fragment>
	)
};

export default ImageResults;