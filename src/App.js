import React from 'react';
import './App.css';
import ImageResults from './components/ImageResults';
import {setSearch, fetching, setResponse} from './actions';
import {connect} from 'react-redux';


const App = (props) => {
  const startSearch = (e) => {
    e.stopPropagation();
    const query = document.getElementById('input').value.toLowerCase().replace(/ /g, '_');
    props.setQuery(query);
    props.fetching(query);
  };

  return (
    <div className="App">
      <h1>Buscador de imágenes</h1>
      <input type='text' id='input' />
      <button onClick={startSearch}> Buscar</button>
      <div className='search'>Resultados para: {props.search}</div>
      { props.isfetching && 'Cargando...'}
      <ImageResults imgs={props.imgs} />
    </div>
  );
}

const mapStateToProps = (state) => ({
  search: state.search,
  isfetching: state.isFetching,
  imgs: state.imgs
});

const mapDispatchToprops = (dispatch) => ({
  setQuery: (search) => {
    dispatch(setSearch(search));
  },
  fetching: (query) => {
    dispatch(fetching());
    fetch(`https://api.unsplash.com/search/photos?page=1&query=${query}?&client_id=AQUI_VA_TU_LLAVE`)
    .then(respose => respose.json())
    .then(json => json.results.map(img => ({
        url: img.urls.small
      })))
    .then(data => {
      dispatch(setResponse(data));
    })
    .finally(() => {
      dispatch(fetching())
    });
  }
});

export default connect(mapStateToProps, mapDispatchToprops)(App);

