import {SET_SEARCH, STARTING_FETCH, SET_RESPONSE} from './constants';

export const setSearch = (search) => ({
	type: SET_SEARCH,
	playload: search
});

export const fetching = () => ({
	type: STARTING_FETCH
});

export const setResponse = (json) => ({
	type: SET_RESPONSE,
	playload: json
})