

// General stuff
import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './index.css';

// Redux functionality
import {createStore, applyMiddleware} from 'redux';
import {appReducer} from './reducers';

// React-Redux 
import {Provider} from 'react-redux';

// Async data flow
import thunk from 'redux-thunk';

const store = createStore(appReducer, {
	search: '',
	isFetching: false,
	imgs: []
}, applyMiddleware(thunk));

ReactDOM.render(
	<Provider store={store}>
		<App/>
	</Provider>, document.getElementById('root'));

